/*!
 * @file CompraParcelada.java
 * @brief Representação para uma compra com parcelamento
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.compra;


/*!
 * @brief Compra que suporta parcelamento
 */
public class CompraParcelada extends Compra {

    private int numeroParcelas; //!< Número de parcelas para compra
    private double taxa;      //<! Valor do juros mensal

    /*!
     * @brief Construtor
     * @param valor            Valor da compra
     * @param numeraParcelas   Quantidade de parcelas da compra
     * @param taxa             Valor juros mensal
     */
    public CompraParcelada(double valor, int numeraParcelas, double taxa) {
        super(valor);
        this.numeroParcelas = numeraParcelas;
        this.taxa = taxa;
    }

    /*!
     * @brief Recupera valor total da compra, aplicando regra de juros composto
     * @return Valor total da compra criada
     */
    @Override
    public double total() {
        double principal = super.total();
        return principal * Math.pow((1 + this.taxa/100), this.numeroParcelas);
    }
}
