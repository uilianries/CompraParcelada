/*!
 * @file Compra.java
 * @brief Representação para uma compra
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.compra;


/*!
 * @brief POD (Plain-Old-Data) Compra
 */
public class Compra {

    private double valorTotal; //!< Valor total da compra

    /*!
     * @brief Construtor
     * @param valor     Recebe valor da compra
     */
    public Compra(double valor) {
        this.valorTotal = valor;
    }

    /*!
     * @brief Recupera valor total da compra
     * @return Valor total da compra criada
     */
    public double total() {
        return this.valorTotal;
    }
}
