/*!
 * @file TestCompraParcelada.java
 * @brief Teste unitário para Compra Parcelada
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.compra;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/*!
 * @brief Executa validação da classe CompraParcelada
 */
public class TestCompraParcelada {

    private CompraParcelada compraParcelada;

    @Test
    public void testCompraParcela1() {
        compraParcelada = new CompraParcelada(38.40, 1, 2.4);
        assertEquals(39.32, compraParcelada.total(), 0.3);
    }

    @Test
    public void testCompraParcela2() {
        compraParcelada = new CompraParcelada(328.90, 2, 3.8);
        assertEquals(354.36, compraParcelada.total(), 0.3);
    }

    @Test
    public void testCompraParcela5() {
        compraParcelada = new CompraParcelada(328.90, 5, 3.8);
        assertEquals(396.29, compraParcelada.total(), 0.3);
    }

    @Test
    public void testCompraParcela10() {
        compraParcelada = new CompraParcelada(328.90, 10, 3.8);
        assertEquals(477.51, compraParcelada.total(), 0.3);
    }

    @Test
    public void testCompraParcela12() {
        compraParcelada = new CompraParcelada(55.20, 12, 5.2);
        assertEquals(101.32, compraParcelada.total(), 0.3);
    }
}
