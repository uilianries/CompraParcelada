/*!
 * @file TestCompra.java
 * @brief Teste unitário para Compra
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
package org.ita.compra;

import org.junit.Test;
;import static org.junit.Assert.assertTrue;

/*!
 * @brief Executa validação da classe compra
 */
public class TestCompra {

    @Test
    public void testValorTotal() {
        final double valor = 55.20;
        Compra compra = new Compra(valor);
        assertTrue(valor == compra.total());
    }
}
